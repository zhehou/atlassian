This readme file is for part 3 of the tasks.

The tests were run on Ubuntu 14.04, Firefox browser is required. The
code is written in Java, compiled in Eclipse 3.8.1. The browser 
version is Firefox 36.0.

Part3.zip contains all the code files and the selenium-server-standalone-2.45.0.jar
lib. Whereas Part3_code.zip only contains the code. 

To run the test, follow the steps below.  

(1) Create a new java project in Eclipse.

(2) Add the .java files in Part3_code.zip to the project.  

(3) Create a directory called lib under the project directory. 

(4) Copy selenium-server-standalone-2.45.0.jar from \lib in Part3.zip
to the created directory in the last step.

(5) Refresh the project directory in Eclipse.

(6) Select Build Path from the right click context menu.

(7) The main function is in TestIssue.java. Run the test.
